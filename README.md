## Opdracht Natural Language Processing
### Introductie
In deze opdracht kijken we naar de _Contextual Abuse Dataset_ (Bertie Vidgen et al., 2021). Dit is een dataset met berichten van social media, die zijn gelabeld als "neutraal" of "abuse", waarbij deze laatste categorie berichten bevat die haatdragend of beledigend zijn ten opzichte van een persoon of groep. Tijdens de opdracht kijken we naar automatische methodes om dergelijke berichten in een van de twee categorieën in te delen.

De originele dataset maakt onderscheid tussen drie verschillende soorten _abuse_, maar voor de opdracht voegen we deze samen tot één categorie.

### Opzet
In de opdracht gaan we eerst proberen een eenvoudige classifier te maken op basis van keywords. Dit wil zeggen: we maken een lijst van woorden die karakteristiek zijn voor _hate speech_, en als een bericht een van deze woorden bevat dan classificeren we het als "abuse" en anders als "neutraal".

Daarna kijken we naar classifiers op basis van Large Language Models/GenAI. We vergelijken de kwaliteit van beide methodes, de voor- en nadelen, en de verschillende soorten fouten die worden gemaakt.

### Praktische informatie
We gebruiken de online omgeving Colab van Google (https://colab.research.google.com/). Klik in het popup-menu op Nieuw Notebook, of linksboven via het menu Bestand de optie Nieuw notebook in Drive. Geef het notebook eventueel een naam. Alternatief kan je de opdracht ook lokaal op je eigen computer uitvoeren in Jupyter Lab.

De code die we gebruiken voor de opdracht staat in deze opdrachtbeschrijving. Om de code uit te voeren moet je in het Notebook op de knop +Code klikken, of via het menu `Invoegen-Codecel`. Kopieer dan een code-blokje uit deze opdrachtbeschrijving en plak deze in de nieuwe cel. Daarna kan je de cel uitvoeren door op de knop Run te klikken, of via het menu `Runtime-De gehighlighte cel uitvoeren` (ctrl-m). Zorg wel dat de cursor in de cel staat die je wil uitvoeren, dan weet het Notebook om welke cel het gaat.

Download de databestanden van de Contextual Abuse Dataset naar je eigen computer en pak het zip-bestand uit:

https://git.science.uu.nl/M.P.Schraagen/rijksoverheid-leergang/-/blob/master/contextualabuse.zip

Upload alle databestanden naar het Notebook via het menu in Colab. Klik op het bestandsicoon links om het bestandsvenster zichtbaar te maken en klik dan op de uploadknop (een rechthoek met een pijltje omhoog).

### Initialisatie
Eerst laden we alle libraries en functies. Maak een nieuwe cel, kopieer de volgende code en voer de cel uit door op de knop Run te klikken. Let op: je kunt deze code **niet uitvoeren vanuit de opdrachtbeschrijving**. Het is de bedoeling dat je de code per blokje _kopieert_ naar het notebook van Colab en het daar uitvoert.

```python
from pprint import pprint
import nltk
from nltk import FreqDist
from nltk.tokenize import word_tokenize
nltk.download('punkt')
import numpy as np
import random
stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]
# stopwords van nltk
```

### Keyword classifier
De volgende code laadt het trainingsgedeelte van de abuse-berichten in. De data wordt op twee manieren opgeslagen: als een lijst met berichten in de variabele `ab_train_lines` en als één lange string in de variabele `ab_train_all`. Maak een nieuw blokje in Colab met de **+Code** knop, kopieer deze code in dit nieuwe blokje en voer het uit.

```python
fin = open("abuse_train.txt")
ab_train_lines = fin.readlines()
fin.close()
ab_train_all = ' '.join(ab_train_lines)
```
**Opdracht 1**

Schrijf zelf code om de overige vijf bestanden uit de dataset in te laden: de development-set en de testset van de abuse-berichten en de train, development en testset van de neutrale berichten. Voor deze data heb je alleen de data als lijst nodig, niet de data als lange string.

**Frequente woorden**  
De volgende code toont de 500 meest voorkomende woorden uit de development-set van de abuse-berichten. De stopwoorden en non-woorden (zoals interpunctie-tekens) worden niet getoond.

```python
abwords = word_tokenize(ab_train_all)
abnostop = [word.lower() for word in abwords if word.lower() not in stopwords and word.isalnum()]
abfreqs = FreqDist(abnostop)
abcommon = abfreqs.most_common(500)
pprint(abcommon)
```
**Opdracht 2**

Kies uit de lijst ongeveer 50 keywords waarvan je denkt dat deze voorkomen in abuse-berichten maar niet in neutrale berichten.

**Opdracht 3**

De volgende code telt het aantal abuse-berichten uit de development-set waarin een woord uit de lijst keywords voorkomt.

Voeg de keywords uit Opdracht 2 toe aan de lijst (en haal de voorbeeld-keywords weg).
```python
keywords = ['aap', 'noot', 'mies']

total_found = 0
for message in ab_development_lines:
    found = False
    for keyword in keywords:
        if keyword in message:
            found = True
    if found:
        total_found += 1
    #else:
    #    print(message)

print("Totaal gevonden (abuse development):", total_found, "/", len(ab_development_lines), "=", total_found/len(ab_development_lines))
```
**Opdracht 4**

Schrijf zelf de code om uit te rekenen in hoeveel van de berichten uit de neutrale development-set de keywords gevonden worden. Reken ook de totale accuracy uit over de combinatie van de abuse-berichten en de neutrale berichten (dit hoeft niet in code, mag wel).

**Opdracht 5**

In de code hierboven staan twee regels in commentaar. Als je het commentaar-teken `#` weghaalt dan wordt elk abuse-bericht uit de development-set getoond waarin geen keyword gevonden is. Bekijk een aantal van deze fouten en probeer extra keywords te vinden om toe te voegen aan de lijst. Reken ook regelmatig de score op de neutrale set opnieuw uit, om te kijken of de toegevoegde keywords inderdaad niet of veel minder voorkomen in de neutrale data. Andersom kan je ook proberen om keywords weg te halen die regelmatig gevonden worden in de neutrale data.

Als je tevreden bent over de performance op de development-set dan kan je de keyword classifier uitvoeren op de testset. Let op: de testset is bedoeld om _onafhankelijk_ de kwaliteit te meten op data die niet is gebruikt bij het maken van de classifier. Je kunt dus _niet_ naar aanleiding van de testresultaten de keywords aanpassen, dit mag _alleen_ naar aanleiding van de development-resultaten.

**Opdracht 6**

Rapporteer de accuracy op de testset voor neutrale berichten, abuse-berichten en alle berichten. Hoeveel verschil is er met de accuracy op de development-set? Is er een verschil tussen de klasses Abuse en Neutraal? Welke van de twee is het belangrijkste om te herkennen, en waarom (dwz. welk probleem ontstaat er als je een verkeerde beslissing neemt)?

De dataset komt uit 2021. Als je de keyword classifier toepast op berichten die in 2024 worden geplaatst, verwacht je dan dat je meer Abuse-berichten onterecht als Neutraal zal zien, of andersom? Waarom? 

### Large Language Models
Nu gaan we hetzelfde classificatieprobleem oplossen maar dan met het large language model Llama 3 (van Meta). Dit is een Generative AI model (vergelijkbaar met ChatGPT) waar je een prompt moet schrijven om de classificatietaak uit te voeren, zoals we in het theorie-gedeelte hebben gezien. Llama 3 is een groot en computationeel zwaar model, dus dit kan je niet eenvoudig op je eigen computer of in een Colab-notebook uitvoeren. In plaats daarvan gebruiken we de API van Groq, dit is een service die gratis toegang aanbiedt tot Llama 3 en waar je vanuit je eigen Python-code prompts naartoe kunt sturen. Je moet daarvoor eerst een persoonlijke _key_ aanmaken op https://console.groq.com/keys. Werk voor deze opdracht in groepjes van twee personen.

Installeer daarna de Groq-library voor Python in een code-blok:
```
!pip install groq
```
Vul daarna de key van je groepje in en initialiseer de library: 
```python
import os
from typing import Dict, List
from groq import Groq

# Get a free API key from https://console.groq.com/keys
os.environ["GROQ_API_KEY"] = "de key van je groepje hier"

LLAMA3_70B_INSTRUCT = "llama-3.1-70b-versatile"
LLAMA3_8B_INSTRUCT = "llama3.1-8b-instant"

DEFAULT_MODEL = LLAMA3_70B_INSTRUCT

client = Groq()
```
Het model werkt het beste als je verschillende _rollen_ gebruikt in de prompt, de _user_ rol voor de input en de _assistant_ rol voor de output. Met de volgende code zet je de rollen klaar en stel je een aantal parameters in:
```python
def assistant(content: str):
    return { "role": "assistant", "content": content }

def user(content: str):
    return { "role": "user", "content": content }

def chat_completion(
    messages: List[Dict],
    model = DEFAULT_MODEL,
    temperature: float = 0.6,
    top_p: float = 0.9,
) -> str:
    response = client.chat.completions.create(
        messages=messages,
        model=model,
        temperature=temperature,
        top_p=top_p,
    )
    return response.choices[0].message.content
```

**Opdracht 7**

Schrijf een **few-shot prompt** voor Llama 3. Hiervoor moet je uit de dataset een aantal voorbeelden selecteren (bv. 4) die je in de prompt kunt toevoegen. Denk goed na welke voorbeelden je wil gebruiken om de LLM op het goede spoor te zetten. Gebruik de volgende basiscode:
```python
def check_abuse(text):
    response = chat_completion(messages=[
        user("You are an online abuse classifier. For each message, give the label abuse or neutral."),
        user(".................. de eerste voorbeeldzin hier .................."),
        assistant("Label: ...eerste label..."),
        user(".................. de tweede voorbeeldzin hier .................."),
        assistant("Label: ...tweede label..."),
         # etc voor de andere voorbeelden
        user(text),   # einde van de prompt waar bij het gebruiken van deze functie een testzin wordt ingevoegd
    ])
    return response

def print_abuse_class(text):
    print(f'INPUT: {text}')
    print(check_abuse(text))
```
**Opdracht 8**

Test de prompt uit met de volgende testzinnen uit de dataset:
```python
print_abuse_class("THey basically spread their dirty ass infestation around with them every time they moved. Trashy af.")
print_abuse_class("These people literally depend on the cops for everything")
```
Probeer zelf ook zinnen uit, vanuit de dataset of zinnen die je zelf schrijft. Zijn er zinnen die incorrect worden geclassificeerd door Llama 3? Zo ja, probeer dan de voorbeelden in de prompt aan te passen en kijk of de zin dan wel goed gaat. **Let op: Groq heeft een quotum van 30 requests per minuut** dus probeer niet teveel, en probeer **zeker niet om een groot deel van de dataset automatisch achter elkaar** te laten classificeren.

**Opdracht 9**

Gebruik de checklist uit het paper van Dong Nguyen (https://aclanthology.org/2021.acl-long.4/, pagina 5 en 7) om een volledig beeld te krijgen van de resultaten van de LLM. Zijn er zinscategorieën of identiteitsgroepen die niet goed geclassificeerd worden?

**Opdracht 10**

Probeer ook een _zero-shot prompt_, dat is dezelfde prompt als hierboven maar dan zonder voorbeelden. Gaat dit ook goed? Bediscussieer de rol van de taak en data: in welke situaties denk je dat een few-shot prompt de grootste voordelen heeft ten opzichte van een zero-shot prompt? Probeer die situatie eventueel ook uit in de code door de prompt en de testzinnen aan te passen aan de nieuwe situatie.

Als je tijd over hebt kan je de uitkomsten van Llama 3 vergelijken met andere LLMs, zoals ChatGPT of Gemini (via de online interfaces van deze LLMs). Maken alle modellen dezelfde fouten?

### Referentie
Vidgen, B., Nguyen, D., Margetts, H., Rossini, P., & Tromble, R. (2021). Introducing CAD: the contextual abuse dataset. In Proceedings of the 2021 Conference of the North American Chapter of the Association for Computational Linguistics: Human Language Technologies (pp. 2289-2303).
